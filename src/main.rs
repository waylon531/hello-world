/* Copyright (c) 2015 The Robigalia Project Developers
 * Licensed under the Apache License, Version 2.0
 * <LICENSE-APACHE or
 * http://www.apache.org/licenses/LICENSE-2.0> or the MIT
 * license <LICENSE-MIT or http://opensource.org/licenses/MIT>,
 * at your option. All files in the project carrying such
 * notice may not be copied, modified, or distributed except
 * according to those terms.
 */
#![no_std]

extern crate sel4_start;
extern crate sel4_sys;

fn println(s: &[u8]) {
    for &b in s {
        unsafe { sel4_sys::seL4_DebugPutChar(b) };
    }
}

fn main() {
    println(b"Hello, world!\n");
}
